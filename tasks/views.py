from django.shortcuts import render, redirect, get_object_or_404
from django.http.response import HttpResponse, HttpResponseNotAllowed
from .models import Task
from .forms import TaskForm

def index(request):
    tasks = Task.objects.all()
    task_form = TaskForm(request.POST or None)

    if request.method == "POST":
        if task_form.is_valid():
            new_task = task_form.save(commit=False)
            new_task.save()
            print(f"{new_task.id=}")
            return redirect("tasks:task-item", id=new_task.id)

        return render(request, "partials/task_form.html", context={
            "task_form": task_form
        })

    context = {
        'tasks': tasks,
        'task_form': task_form,
    }

    return render(request, 'tasks/index.html', context)

def create_task_form(request):
    task_form = TaskForm()

    context = {
        'task_form': task_form,
    }

    return render(request, "partials/task_form.html", context)

def task_item(request, id):
    task = get_object_or_404(Task, id=id)

    context = {
        "task": task
    }

    return render(request, "partials/task_item.html", context)

def task_detailed(request, id):
    task = get_object_or_404(Task, id=id)

    context = {
        "task": task
    }

    return render(request, "partials/task_detailed.html", context)

def task_update(request, id):
    task = get_object_or_404(Task, id=id)
    task_form = TaskForm(request.POST or None, instance=task)

    if request.method == "POST":
        if task_form.is_valid():
            task_form.save()
            return redirect("tasks:task-item", id=task.id)

    context = {
        'task': task,
        'task_form': task_form,
    }

    return render(request, "partials/task_form.html", context)

def task_set_circular_next_status(request, id):
    task = get_object_or_404(Task, id=id)

    if request.method == "POST":
        task.set_circular_next_status()

    context = {
        'task': task,
    }

    return render(request, "partials/task_item.html", context)

def task_delete(request, id):
    task = get_object_or_404(Task, id=id)

    if request.method == "POST":
        task.delete()
        return HttpResponse("")
    return HttpResponseNotAllowed([ "POST" ])

