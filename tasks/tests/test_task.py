from django.test import TestCase
from tasks.models import Task

class TaskTestCase(TestCase):
    def setUp(self):
        statuses = ["TODO", "DOING", "DONE", "CANCELED"]
        names = [f"Test Task {i}" for i in statuses]
        description = ""

        tasks = [
            Task(name=name, description=description, status=status)
            for name, status in zip(names, statuses)
        ]

        Task.objects.bulk_create(tasks)

    def test__get_status_index_with_existing(self):
        task = Task.objects.filter(status="DONE").first()
        index = task._get_status_index()
        self.assertEqual(index, 2)

    def test_circular_next_status_index_for_todo(self):
        task = Task.objects.filter(status="TODO").first()
        index = task.circular_next_status_index()
        self.assertEqual(index, 1)

    def test_circular_next_status_index_for_canceled(self):
        task = Task.objects.filter(status="CANCELED").first()
        next_index = task.circular_next_status_index()
        self.assertEqual(next_index, 0)

    def test_circular_next_status_for_todo(self):
        task = Task.objects.filter(status="TODO").first()
        next_status = task.circular_next_status()
        self.assertEqual(next_status, "DOING")

    def test_circular_next_status_for_canceled(self):
        task = Task.objects.filter(status="CANCELED").first()
        next_status = task.circular_next_status()
        self.assertEqual(next_status, "TODO")

    def test_set_circular_next_status_for_todo(self):
        task = Task.objects.filter(status="TODO").first()
        task.set_circular_next_status()
        task.refresh_from_db()
        self.assertEqual(task.status, "DOING")

    def test_set_circular_next_status_for_canceled(self):
        task = Task.objects.filter(status="CANCELED").first()
        task.set_circular_next_status()
        task.refresh_from_db()
        self.assertEqual(task.status, "TODO")

