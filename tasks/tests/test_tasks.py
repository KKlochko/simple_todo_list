from django.test import TestCase
from tasks.models import Task, Tasks

class TasksTestCase(TestCase):
    todo_count = 2
    doing_count = 3
    done_count = 4
    cancel_count = 1

    def setUp(self):
        names = [f"Test Task {i}" for i in range(10)]
        description = ""

        statuses = ["TODO"] * self.todo_count
        statuses += ["DOING"] * self.doing_count
        statuses += ["DONE"] * self.done_count
        statuses += ["CANCELED"] * self.cancel_count

        tasks = [
            Task(name=name, description=description, status=status)
            for name, status in zip(names, statuses)
        ]

        Task.objects.bulk_create(tasks)

    def test_done_count(self):
        count = Tasks.get_count_by_status("DONE")
        self.assertEqual(count, self.done_count)

