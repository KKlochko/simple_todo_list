from django.db import models

class Task(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField(max_length=5000)

    # Constants for status
    TODO = "TODO"
    DOING = "DOING"
    DONE = "DONE"
    CANCELED = "CANCELED"
    STATUSES = [
        (TODO, "TODO"),
        (DOING, "DOING"),
        (DONE, "DONE"),
        (CANCELED, "CANCELED"),
    ]

    status = models.CharField(
        max_length=8,
        choices=STATUSES,
        default=TODO,
    )

    def __str__(self):
        return self.name

    @staticmethod
    def get_statuses():
        return list(map(lambda _: _[1], Task.STATUSES))

    def _get_status_index(self):
        return Task.get_statuses().index(self.status)

    def circular_next_status_index(self):
        current_index = self._get_status_index()
        next_index = (current_index + 1) % len(Task.STATUSES)
        return next_index

    def circular_next_status(self):
        next_index = self.circular_next_status_index()
        return Task.get_statuses()[next_index]

    def set_circular_next_status(self):
        self.status = self.circular_next_status()
        self.save()

class Tasks:
    @staticmethod
    def get_count_by_status(status: str):
        return Task.objects.filter(status=status).count()

