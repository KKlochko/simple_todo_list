from django.conf.urls import include
from django.urls import path

from . import views

app_name = 'tasks'

urlpatterns = [
    path('', views.index, name='index'),
    path('htmx/create-task-form/', views.create_task_form, name='create-task-form'),
    path('htmx/task-item/<id>/', views.task_item, name='task-item'),
    path('htmx/task-item/<id>/detailed/', views.task_detailed, name='task-detailed'),
    path('htmx/task-item/<id>/update/', views.task_update, name='task-update'),
    path('htmx/task-item/<id>/set-circular-next-status/',
         views.task_set_circular_next_status,
         name='task-set-circular-next-status'),
    path('htmx/task-item/<id>/delete/', views.task_delete, name='task-delete'),
]

