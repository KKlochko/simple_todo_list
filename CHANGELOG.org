* Change Log
** 0.1.0 <2023-07-12 Wed>
   Init project.
** 0.2.0 <2023-07-12 Wed>
   Add simple tasks app.
** 0.2.1 <2023-07-12 Wed>
   Add the tasks to be shown on the index page.
** 0.2.2 <2023-07-13 Thu>
   Add the TailwindCSS.
** 0.2.3 <2023-07-14 Fri>
   Update the main container block.
** 0.2.4 <2023-07-14 Fri>
   Update the table to a list of items and the header.
** 0.3.0 <2023-07-15 Sat>
   Add the htmx (CDN version).
   Add the task form.
   Add logic to swap the form with the task via htmx.
** 0.3.1 <2023-07-17 Mon>
   Add the delete button for a task.
** 0.3.2 <2023-07-20 Thu>
   Add the update button for a task.
** 0.3.3 <2023-07-21 Fri>
   Add the cancel button for a task form.
** 0.3.4 <2023-07-21 Fri>
   Add right alignment for buttons in a task item.
** 0.3.5 <2023-07-22 Sat>
   Update the style of buttons.
** 0.3.6 <2023-07-23 Sun>
   Add the circular next methods for Task.
   Add the tests for circular next methods.
** 0.3.7 <2023-07-23 Sun>
   Add the action to set the circular next status for Task items.
** 0.3.8 <2023-07-24 Mon>
   Update the database to use Postgres.
** 0.3.9 <2023-07-24 Mon>
   Add the CI/CD configuration.
** 0.3.10 <2023-07-24 Mon>
   Add the Tasks model to manage tasks.
** 0.4.0 <2023-07-24 Mon>
   Add the detailed item for a Task model.

