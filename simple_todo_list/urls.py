from django.contrib import admin
from django.urls import include, path
from .settings import DEBUG

urlpatterns = [
    path('', include('tasks.urls')),
    path("admin/", admin.site.urls),
]

if DEBUG:
    urlpatterns.append(path("__reload__/", include("django_browser_reload.urls")))

